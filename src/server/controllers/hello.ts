export const helloController: Dictionary<RouteMethod> = {
  getHello: ( request, f, err ) =>
    err
      ? { errorCode: err.name, errorMessage: err.message }
      : { message: 'hello world from server' },
};
