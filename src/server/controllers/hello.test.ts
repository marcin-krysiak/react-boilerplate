import { helloController } from './hello';

describe( 'hello controller', () => {
  const getHello: Function = helloController.getHello;
  it( 'getHello to handle error response with error message', () =>
    expect( getHello( null, null, { name: 'bad error', message: 'some very bad error' } ) )
      .toEqual( { errorMessage: 'some very bad error', errorCode: 'bad error' } ),
  );

  it( 'getHello to handle error response with error message', () =>
    expect( getHello() ).toEqual( { message: 'hello world from server' } ),
  );
} );
