import { ServerRoute } from 'hapi';
import { pingController } from '../controllers/ping';

export const ping: ServerRoute[] = [{
  method: 'GET',
  path: `ping`,
  handler: pingController.getPing,
}, {
  method: 'POST',
  path: `ping`,
  handler: pingController.postPing,
}];
