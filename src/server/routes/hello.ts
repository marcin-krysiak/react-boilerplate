import { ServerRoute } from 'hapi';
import { helloController } from '../controllers/hello';
import { helloValidate } from '../validate/hello';

export const hello: ServerRoute = {
  method: 'GET',
  path: `hello`,
  handler: helloController.getHello,
  options: {
    validate: helloValidate.getHello,
  },
};
