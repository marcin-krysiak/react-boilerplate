import { readdirSync } from 'fs';
import { ServerRoute } from 'hapi';

let routesArray: Dictionary<ServerRoute> = {};

readdirSync( __dirname )
  .filter( file => file !== 'index.ts' )
  .forEach( file => routesArray = { ...routesArray, ...require( `./${file}` ) } );

export const routes: Dictionary<ServerRoute> = routesArray;
