import { Lifecycle } from 'hapi';

declare global {
  export type Dictionary<T> = { [key: string]: T };
  export type RouteMethod = Lifecycle.Method;

  export interface PayloadAction<T> {
    type: string;
    payload?: T;
  }
}
