import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import { reducers } from './redux/reducers';
import { DEFAULT_STATE } from './redux/state';
import { history } from './index';

export const configureStore = ( initialState = DEFAULT_STATE ) =>
  createStore(
    reducers,
    initialState,
    applyMiddleware(
      reduxThunk,
      reduxLogger,
      routerMiddleware(history),
    ),
  );
