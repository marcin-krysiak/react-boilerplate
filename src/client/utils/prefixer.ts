const Prefixer = require( 'inline-style-prefixer' );

type CSSProperties = React.CSSProperties | { [key: string]: React.CSSProperties };

const prefixer = new Prefixer();
export const prefix =
  <CSSProperties>( style: CSSProperties ): CSSProperties => prefixer.prefix( style );
