import { combineReducers } from 'redux';
import { helloMessage } from './branches/getHello';
import { routerReducer } from 'react-router-redux';

export const reducers = combineReducers({
  helloMessage,
  router: routerReducer,
});
