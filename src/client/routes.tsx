import * as React from 'react';
import { Route } from 'react-router';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';
import App from './components/App/App';
import { AlertDialog } from './components/Dialog/AlertDialog';
import { history } from './index';

export const AppRouter = () =>
  <ConnectedRouter history={history}>
    <div>
      <Route exact path="/" component={App}/>
      <Route exact path="/dialog" component={AlertDialog}/>
    </div>
  </ConnectedRouter>;
