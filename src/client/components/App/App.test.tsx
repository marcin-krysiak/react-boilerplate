import * as React from 'react';
import { shallow } from 'enzyme';
import { App, Props } from './App';

describe('App Component', () => {
  const mockedProps: Props = {
    helloMessage: 'some message',
    getHelloMessage: () => null,
    goToDialog: () => null,
  };

  it('should render without crashing', () => {
    expect(
      shallow(<App {...mockedProps} />)
        .find('div').length,
    ).toBe(1);
  });
});
