export const styles = {
  colors: {
    blue: '#0000ff',
    yellow: '#ffff00',
    red: '#ff0000',
    black: '#000000',
  },
  gutter: {
    small: '0.5rem',
    normal: '1rem',
    large: '2rem',
  },
};
